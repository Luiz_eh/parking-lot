import { Component, OnInit } from '@angular/core';
import { Parking } from '../model/parking.vehicle';

const MILIS_TO_HOURS = 1000 * 60 * 60
const PARKING_RATE = 4.5

@Component({
    selector: 'app-list-parking',
    templateUrl: './list-parking.component.html',
    styleUrls: ['./list-parking.component.css']
})
export class ListParkingComponent implements OnInit {

    parkings: Parking[] = []

    ngOnInit(): void {
        this.loadListFromStorage()
    }

    endParking(parking: Parking) {
        let startDate = new Date(parking.start)
        let finishDate = new Date()

        let durationInHours = (finishDate.getTime() - startDate.getTime()) / MILIS_TO_HOURS
        parking.price = durationInHours * PARKING_RATE
        parking.finish = finishDate

        localStorage.setItem('parking-lot', JSON.stringify(this.parkings))
    }

    loadListFromStorage() {
        let list = localStorage.getItem('parking-lot')
        if (list != null) {
            this.parkings = JSON.parse(list)
        }
    }

}
